﻿from imutils.video import VideoStream
import cv2
from pyfirmata import Arduino, util
from threading import Thread
from time import sleep
from math import fabs

board = None
try:
   board = Arduino('/dev/ttyUSB0')
   print "Arduino on ttyUSB0"
except:
   try:
      board = Arduino('/dev/ttyUSB1')
      print "Arduino on ttyUSB1"
   except:
      print "Could not find Arduino. Quitting."
      quit()
      
runServoThread = True
posCenter = 80
posTarget = posCenter
posServo = posCenter
maxAmp = 90
vSweepMin = 1.5
vSweepMax = 10
vSweep = vSweepMin
vFace = (0, 0) # Face speed (for position prediction)
posFacePrev = None

cascPath = "haarcascade_frontalface_default.xml"
faceCascade = cv2.CascadeClassifier(cascPath)
font = cv2.FONT_HERSHEY_SIMPLEX

# Start an iterator thread so
# Serial buffer doesn't overflow
iterator = util.Iterator(board)
iterator.start()

# Set up pin D9 as Servo Output
pin9 = board.get_pin('d:9:s')
pin9.write(posCenter)

# Servo position is set in separate thread
class servoThread(Thread):
   def __init__(self):
      Thread.__init__(self)
      self.daemon = True
      self.start()
   def run(self):
      global posServo
      global vFace
      while runServoThread:
         if posTarget < posServo:
            posServo -= vSweep
         elif posTarget > posServo:
            posServo += vSweep
         if fabs(posTarget - posServo) < vSweep:
            posServo = posTarget
         pin9.write(posServo)
         sleep(0.05)
         # Reduce face speed a bit
         # vFace = (vFace[0] * 0.7, vFace[1] * 0.7)
         
# start servo thread
servoThread()

# Initialize the video stream and allow the camera sensor to warm up
print("[INFO] starting video stream...")
size = (640, 480)
vs = VideoStream(usePiCamera=True, resolution=size).start()
sleep(1.0)
center = (320, 240)

# Loop over the frames from the video stream
while True:
   # Grab the frame from the video stream
   frame = vs.read()

   # Rotate 180° (camera is top down)
   M = cv2.getRotationMatrix2D(center, 180, 1.0)
   frame = cv2.warpAffine(frame, M, size)
   
   # Convert to grayscale
   gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

   faces = faceCascade.detectMultiScale(
      gray,
      scaleFactor=1.1,
      minNeighbors=5,
      minSize=(40, 40),
      flags=cv2.CASCADE_SCALE_IMAGE)

   # Draw a rectangle around detected faces
   # for (x, y, w, h) in faces:
   #   cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 100), 1)
   #   strXY = "%s, %s" % (x, y)
   #   cv2.putText(frame, strXY, (x, y + h + 12), font, 0.45, (0, 0, 255), 1)
      
   # Show the output frame
   # cv2.imshow("Frame", frame)
   # key = cv2.waitKey(1) & 0xFF

   # Calculate new target servo position
   if hasattr(faces, 'size') and faces.size:
      # Calculate current face velocity
      # if posFacePrev is not None:
      #   vFace = (faces[0][0] - posFacePrev[0], faces[0][1] - posFacePrev[1])
      # Debug output
      # print "Vertical face speed: " + str(vFace[0])
      posTarget = posCenter + ((max(0, min(640, (faces[0][0] + vFace[0]))) / 640.0) * maxAmp - (maxAmp / 2))      
      posFacePrev = (faces[0][0], faces[0][1])
      # Adapt sweeping speed to distance between target position
      # and current servo position
      vSweep = vSweepMin + (abs(posTarget - posServo) / maxAmp) * (vSweepMax - vSweepMin)
      posTarget = int(round(posTarget))

   # If the 'esc' key was pressed, break from the loop
   # if key == 27:
   #   break

# Do a bit of cleanup
cv2.destroyAllWindows()
vs.stop()
runServoThread = False
